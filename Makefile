LISP ?= sbcl

build:
	$(LISP) --load qwer.asd \
	--eval '(ql:quickload :qwer)' \
	--eval '(asdf:make :qwer)' \
	--eval '(quit)'
